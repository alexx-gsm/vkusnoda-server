const bodyParser = require('body-parser')
const cors = require('cors')
const passport = require('passport')

module.exports = (app) => {
    // cors middleware
    app.use(cors())
    // Body parser middleware
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(bodyParser.json())

    // Passport middleware
    app.use(passport.initialize())
    // Passport config
    require('../config/passport')(passport)
}
