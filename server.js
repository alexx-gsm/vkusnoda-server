const express = require('express')
const mongoose = require('mongoose')
const routes = require('./routes')
const middlewares = require('./middlewares')
const http = require('http')
const socketIO = require('socket.io')
const socketioJwt = require('socketio-jwt')

// app
const app = express()

// middlewares
middlewares(app)

// routes
routes(app)

// mongodb
const { mongoURI, options } = require('./config/db')
mongoose
    .connect(mongoURI, options)
    .then(() => console.log('MongoDB Connected...'))
    .catch((err) => console.log(err))

// io
const server = http.createServer(app)
const io = socketIO(server)
const jwtsecret = require('./config/keys').secretOrKey

const socketHandler = require('./socket')

io.on(
    'connection',
    socketioJwt.authorize({
        secret: jwtsecret,
        timeout: 15000,
    })
)
    .on('authenticated', (socket) => {
        console.log('SOCKET-CONNECT:', `${socket.decoded_token.email} at ${new Date()}`)
        socket.on('disconnect', () =>
            console.log(`SOCKET-DISCONNECT: ${socket.decoded_token.email}`)
        )

        socketHandler(socket, io)
    })
    .on('error', () => console.log('error', error))

// fire server
const port = process.env.PORT || 5005
server.listen(5005, () => console.log(`is running on port ${port}...`))
