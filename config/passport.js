const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const User = require('../models/User')
const { secretOrKey } = require('./keys')

const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey,
}

module.exports = (passport) => {
    passport.use(
        new JwtStrategy(options, (jwt_payload, done) => {
            User.findById(jwt_payload._id)
                .then((user) => {
                    if (user) {
                        return done(null, user)
                    }
                    return done(null, false)
                })
                .catch((err) => console.log(err))
        })
    )
}
