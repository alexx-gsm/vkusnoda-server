module.exports = [
    { model: 'Dish', eventName: 'dishes' },
    { model: 'Courier', eventName: 'couriers' },
    { model: 'Client', eventName: 'clients' },
    { model: 'Category', eventName: 'categories' },
    { model: 'Uom', eventName: 'uoms' },
    { model: 'Menu', eventName: 'menus' },
    { model: 'Output', eventName: 'outputs' },
    { model: 'Order', eventName: 'orders' },
]
