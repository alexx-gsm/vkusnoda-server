const entities = require('./entities')
const answer = require('../helpers/socket-answer')

module.exports = (socket, io) => {
    entities.map(({ model }) => {
        socket.on(`getItemsOf${model}`, async (options, cb) => {
            const Model = require(`../models/${model}`)
            const items = await Model.find({ isDeleted: false, ...options })

            answer(cb, items)
        })
    })

    socket.on('save', async ({ model, eventName, data, options = {} }, cb) => {
        const Model = require(`../models/${model}`)
        const validator = require(`../models/${model}/validation/input`)

        const { errors, isValid } = validator(data)

        if (!isValid) {
            answer(cb, { errors })
            return
        }

        const { _id, author, ...itemFields } = data

        let item = {}

        try {
            if (_id) {
                item = await Model.findOneAndUpdate(
                    { _id },
                    { $set: { ...itemFields, updated: new Date() } },
                    { new: true, upsert: true }
                )
                answer(cb, item)
            } else {
                const filter = options.filter || null
                const number = options.hasNumber ? await Model.countDocuments(filter) : null

                item = await new Model({
                    ...itemFields,
                    author: socket.decoded_token._id,
                })

                if (number !== null) {
                    Object.assign(item, {
                        number: options.preNumber
                            ? `${options.preNumber}${number + 1}`
                            : number + 1,
                    })
                }

                await item.save()

                answer(cb, item)
            }

            io.emit(`updated_${eventName}`, item)
        } catch (errors) {
            console.log('error', errors)
            answer(cb, { errors })
        }
    })
}
