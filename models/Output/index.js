const mongoose = require('mongoose')
const Schema = mongoose.Schema

const OutputSchema = new Schema({
    date: {
        type: Date,
        default: new Date(),
    },
    menuId: {
        type: Schema.Types.ObjectId,
        ref: 'menu',
        required: true,
    },
    dishes: [
        {
            dishId: String,
            title: {
                type: String,
                default: '',
            },
            categoryId: {
                type: Schema.Types.ObjectId,
                ref: 'category',
            },
            weight: {
                type: Number,
                require: true,
            },
            uomId: {
                type: Schema.Types.ObjectId,
                ref: 'uom',
            },
            price: {
                type: Number,
                require: true,
            },
            doneAmount: {
                type: Number,
                default: 0,
            },
            outlookAmount: {
                type: Number,
                default: 0,
            },
        },
    ],
    isDeleted: {
        type: Boolean,
        default: false,
    },
    created: {
        type: Date,
        default: new Date(),
    },
    updated: {
        type: Date,
        default: new Date(),
    },
    author: {
        type: Schema.Types.ObjectId,
        rel: 'user',
    },
})

module.exports = Output = mongoose.model('output', OutputSchema)
