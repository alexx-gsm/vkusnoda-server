const Validator = require('validator')
const isEmpty = require('../../../helpers/is-empty')

module.exports = function(data) {
    let errors = {}

    // menuId
    data.menuId = !isEmpty(data.menuId) ? data.menuId : ''
    if (Validator.isEmpty(data.menuId)) {
        errors.menuId = 'is required'
    }

    // dishes
    data.dishes = !isEmpty(data.dishes) ? data.dishes : []

    // author
    data.author = !isEmpty(data.author) ? data.author : []

    return {
        errors,
        isValid: isEmpty(errors),
    }
}
