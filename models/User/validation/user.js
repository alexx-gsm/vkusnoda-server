const Validator = require('validator')
const isEmpty = require('../../../helpers/is-empty')

module.exports = function validateUserInput(data) {
    let errors = {}

    // user's name
    data.name = !isEmpty(data.name) ? data.name : ''
    if (!Validator.isLength(data.name, { min: 3, max: 50 })) {
        errors.name = 'ФИО от 3 до 50 символов'
    }
    if (Validator.isEmpty(data.name)) {
        errors.name = 'ФИО обязательно'
    }

    // company
    data.company = !isEmpty(data.company) ? data.company : ''

    // contact phone
    data.phone = !isEmpty(data.phone) ? data.phone : ''
    if (Validator.isEmpty(data.phone)) {
        errors.phone = 'Необходимо указать контактный телефон'
    }

    // address
    data.address = !isEmpty(data.address) ? data.address : ''
    if (Validator.isEmpty(data.address)) {
        errors.address = 'Необходимо указать адрес доставки'
    }

    // map
    data.map = !isEmpty(data.map) ? data.map : ''
    if (data.map.length > 0 && !Validator.isURL(data.map)) {
        errors.map = 'Неверный формат ссылки'
    }

    // comment
    data.comment = !isEmpty(data.comment) ? data.comment : ''

    // // email
    // data.email = !isEmpty(data.email) ? data.email : '';
    // // if (Validator.isEmpty(data.email)) {
    // //   errors.email = 'Email field is required';
    // // }
    // if (!Validator.isEmpty(data.email) && !Validator.isEmail(data.email)) {
    //   errors.email = 'Укажите корректный e-mail или оставьте поле пустым';
    // }

    return {
        errors,
        isValid: isEmpty(errors),
    }
}
