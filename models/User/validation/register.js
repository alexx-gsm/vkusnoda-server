const Validator = require('validator')
const isEmpty = require('../../../helpers/is-empty')

module.exports = function validateRegisterInput(data) {
    let errors = {}

    // email
    data.email = !isEmpty(data.email) ? data.email : ''
    if (Validator.isEmpty(data.email)) {
        errors.email = 'Email обязательно'
    }
    if (!Validator.isEmail(data.email)) {
        errors.email = 'Email не верный формат'
    }

    // password
    data.password = !isEmpty(data.password) ? data.password : ''
    if (Validator.isEmpty(data.password)) {
        errors.password = 'Пароль обязателен'
    }
    if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
        errors.password = 'Пароль от 6 знаков'
    }

    // password 2
    data.password2 = !isEmpty(data.password2) ? data.password2 : ''
    if (Validator.isEmpty(data.password2)) {
        errors.password2 = 'Пароль обязателен'
    }
    if (!Validator.equals(data.password, data.password2)) {
        errors.password2 = 'Пароли не совпадают'
    }

    return {
        errors,
        isValid: isEmpty(errors),
    }
}
