const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
    name: {
        type: String,
    },
    email: {
        type: String,
        require: true,
    },
    hash: {
        type: String,
        require: true,
    },
    role: {
        type: String,
        default: 'user',
    },
    avatar: {
        type: String,
        default: '',
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
    created: {
        type: Date,
        default: new Date(),
    },
    updated: {
        type: Date,
        default: new Date(),
    },
})

module.exports = User = mongoose.model('user', UserSchema)
