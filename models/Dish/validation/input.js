const Validator = require('validator')
const isEmpty = require('../../../helpers/is-empty')

module.exports = function(data) {
    let errors = {}

    // title
    data.title = !isEmpty(data.title) ? data.title : ''
    if (!Validator.isLength(data.title, { min: 2, max: 50 })) {
        errors.title = 'title must have at least 2 chars'
    }
    if (Validator.isEmpty(data.title)) {
        errors.title = 'title is required'
    }

    // cardTitle
    data.cardTitle = !isEmpty(data.cardTitle) ? data.cardTitle : data.title

    // category
    data.categoryId = !isEmpty(data.categoryId) ? data.categoryId : ''
    if (Validator.isEmpty(data.categoryId)) {
        errors.categoryId = 'is required'
    }

    // isComplex
    data.isComplex = !isEmpty(data.isComplex) ? data.isComplex : false

    // weight
    data.weight = !isEmpty(data.weight) ? +data.weight : 0
    if (data.weight === 0) {
        errors.weight = 'is required'
    }

    // uom
    data.uomId = !isEmpty(data.uomId) ? data.uomId : ''
    if (Validator.isEmpty(data.uomId)) {
        errors.uomId = 'is required'
    }

    // price
    data.price = !isEmpty(data.price) ? +data.price : 0
    if (data.price === 0) {
        errors.price = 'is required'
    }

    // composition
    data.composition = !isEmpty(data.composition) ? data.composition : ''

    // comment
    data.comment = !isEmpty(data.comment) ? data.comment : ''

    // checklist
    data.checklist = !isEmpty(data.checklist) ? data.checklist : ''

    return {
        errors,
        isValid: isEmpty(errors),
    }
}
