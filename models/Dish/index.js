const mongoose = require('mongoose')
const Schema = mongoose.Schema

const DishSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    cardTitle: {
        type: String,
        required: true,
    },
    categoryId: {
        type: Schema.Types.ObjectId,
        ref: 'category',
        required: true,
    },
    isComplex: {
        type: Boolean,
        default: false,
    },
    weight: {
        type: Number,
        required: true,
    },
    uomId: {
        type: Schema.Types.ObjectId,
        ref: 'uom',
    },
    price: {
        type: Number,
        required: true,
    },
    composition: {
        type: String,
        default: '',
    },
    comment: {
        type: String,
        default: '',
    },
    checklist: {
        type: String,
        default: '',
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
    created: {
        type: Date,
        default: new Date(),
    },
    updated: {
        type: Date,
        default: new Date(),
    },
})

module.exports = Dish = mongoose.model('dish', DishSchema)
