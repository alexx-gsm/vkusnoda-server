const mongoose = require('mongoose')
const Schema = mongoose.Schema

const OrderSchema = new Schema({
    date: {
        type: Date,
        required: true,
    },
    number: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        required: true,
    },
    client: {
        type: Schema.Types.ObjectId,
        ref: 'client',
    },
    organization: {
        type: String,
        default: '',
    },
    address: {
        type: String,
        required: true,
    },
    phones: [
        {
            type: String,
            require: true,
        },
    ],
    courier: {
        type: Schema.Types.ObjectId,
        ref: 'courier',
    },
    payment: {
        type: String,
        required: true,
    },
    isPaid: {
        type: Boolean,
        default: false,
    },
    discount: {
        type: Number,
        default: 0,
    },
    dishes: [
        {
            _id: { type: Schema.Types.ObjectId, ref: 'dish' },
            title: {
                type: String,
                require: true,
            },
            amount: {
                type: Number,
                require: true,
            },
            categoryId: {
                type: Schema.Types.ObjectId,
                ref: 'category',
            },
            weight: {
                type: Number,
                require: true,
            },
            uomId: {
                type: Schema.Types.ObjectId,
                ref: 'uom',
            },
            price: {
                type: Number,
                require: true,
            },
            isComplex: {
                type: Boolean,
                require: true,
            },
            complexDish: {
                _id: { type: Schema.Types.ObjectId, ref: 'dish' },
                title: {
                    type: String,
                    require: true,
                },
                categoryId: {
                    type: Schema.Types.ObjectId,
                    ref: 'category',
                },
                weight: {
                    type: Number,
                    require: true,
                },
                uomId: {
                    type: Schema.Types.ObjectId,
                    ref: 'uom',
                },
                price: {
                    type: Number,
                    require: true,
                },
            },
        },
    ],
    comment: {
        type: String,
        default: '',
    },
    commentToKitchen: {
        type: String,
        default: '',
    },
    commentToCourier: {
        type: String,
        default: '',
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
    created: {
        type: Date,
        default: new Date(),
    },
    updated: {
        type: Date,
        default: new Date(),
    },
    author: {
        type: Schema.Types.ObjectId,
        rel: 'user',
    },
})

module.exports = Order = mongoose.model('order', OrderSchema)
