const Validator = require('validator')
const isEmpty = require('../../../helpers/is-empty')

module.exports = function(data) {
    let errors = {}

    // date
    data.date = !isEmpty(data.date) ? data.date : ''
    if (Validator.isEmpty(data.date)) {
        errors.date = 'is required'
    }

    // number
    data.number = !isEmpty(data.number) ? data.number : ''
    // if (Validator.isEmpty(data.number)) {
    //     errors.number = 'is required'
    // }

    // status
    data.status = !isEmpty(data.status) ? data.status : ''
    if (Validator.isEmpty(data.status)) {
        errors.status = 'is required'
    }

    // client
    data.client = !isEmpty(data.client) ? data.client : ''
    if (Validator.isEmpty(data.client)) {
        errors.client = 'is required'
    }

    // address
    data.address = !isEmpty(data.address) ? data.address : ''
    if (Validator.isEmpty(data.address)) {
        errors.address = 'is required'
    }

    // phones
    data.phones = !isEmpty(data.phones) ? data.phones : []
    if (isEmpty(data.phones)) {
        errors.phones = 'is required'
    }

    // courier
    data.courier = !isEmpty(data.courier) ? data.courier : ''
    if (Validator.isEmpty(data.courier)) {
        errors.courier = 'is required'
    }

    // organization
    data.organization = !isEmpty(data.organization) ? data.organization : ''

    // payment
    data.payment = !isEmpty(data.payment) ? data.payment : 'cash'

    // isPaid
    data.isPaid = !isEmpty(data.isPaid) ? data.isPaid : false

    // discount
    data.discount = !isEmpty(data.discount) ? data.discount : 0

    // dishes
    data.dishes = !isEmpty(data.dishes) ? data.dishes : []
    if (isEmpty(data.dishes)) {
        errors.dishes = 'is required'
    }

    // complexDish
    data.complexDish = !isEmpty(data.complexDish) ? data.complexDish : {}

    // comment
    data.comment = !isEmpty(data.comment) ? data.comment : ''

    // commentToKitchen
    data.commentToKitchen = !isEmpty(data.commentToKitchen) ? data.commentToKitchen : ''

    // commentToCourier
    data.commentToCourier = !isEmpty(data.commentToCourier) ? data.commentToCourier : ''

    return {
        errors,
        isValid: isEmpty(errors),
    }
}
