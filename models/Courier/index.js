const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CourierSchema = new Schema({
    title: {
        type: String,
        require: true,
    },
    // customers: [
    //     {
    //         customer: {
    //             type: Schema.Types.ObjectId,
    //             ref: 'customer',
    //         },
    //         sorting: {
    //             type: String,
    //             default: '0',
    //         },
    //     },
    // ],
    isDeleted: {
        type: Boolean,
        default: false,
    },
    created: {
        type: Date,
        default: new Date(),
    },
    updated: {
        type: Date,
        default: new Date(),
    },
})

module.exports = Courier = mongoose.model('courier', CourierSchema)
