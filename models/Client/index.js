const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ClientSchema = new Schema({
    name: {
        type: String,
        require: true,
    },
    organization: {
        type: String,
        default: 'ФЛ',
    },
    number: {
        type: String,
        default: '',
    },
    acronym: {
        type: String,
        default: '',
    },
    isGroup: {
        type: Boolean,
        default: false,
    },
    parent: {
        type: String,
        default: '',
    },
    phones: [
        {
            type: String,
            require: true,
        },
    ],
    address: {
        type: String,
        require: true,
    },

    courier: {
        type: Schema.Types.Mixed,
        // ref: 'courier',
        default: '',
    },
    payment: {
        type: String,
        required: true,
    },
    discount: {
        type: Number,
        default: 0,
    },
    comment: {
        type: String,
        default: '',
    },
    notes: [
        {
            type: String,
            default: '',
        },
    ],
    isDeleted: {
        type: Boolean,
        default: false,
    },
    created: {
        type: Date,
        default: new Date(),
    },
    updated: {
        type: Date,
        default: new Date(),
    },
})

module.exports = Client = mongoose.model('client', ClientSchema)
