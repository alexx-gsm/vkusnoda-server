const Validator = require('validator')
const isEmpty = require('../../../helpers/is-empty')

module.exports = function(data) {
    let errors = {}

    // isGroup
    data.isGroup = !isEmpty(data.isGroup) ? data.isGroup : false

    // name
    data.name = !isEmpty(data.name) ? data.name : ''
    if (!data.isGroup && !Validator.isLength(data.name, { min: 3, max: 50 })) {
        errors.name = 'name must have at least 3 chars'
    }
    if (!data.isGroup && Validator.isEmpty(data.name)) {
        errors.name = 'is required'
    }

    // organization
    data.organization = !isEmpty(data.organization) ? data.organization : ''
    if (Validator.isEmpty(data.organization)) {
        errors.organization = 'is required'
    }
    // if (data.isGroup && !Validator.isLength(data.organization, { min: 3, max: 50 })) {
    //     errors.organization = 'organization must have at least 3 chars'
    // }

    // acronym
    data.acronym = !isEmpty(data.acronym) ? data.acronym : ''

    // number
    data.number = !isEmpty(data.number) ? data.number : ''

    // parent
    data.parent = !isEmpty(data.parent) ? data.parent : ''

    // phones
    data.phones = !isEmpty(data.phones) ? data.phones : []
    if (!data.isGroup && isEmpty(data.phones)) {
        errors.phones = 'is required'
    }

    // address
    data.address = !isEmpty(data.address) ? data.address : ''
    if (!data.isGroup && Validator.isEmpty(data.address)) {
        errors.address = 'is required'
    }

    // courier
    data.courier = !isEmpty(data.courier) ? data.courier : ''
    if (!data.isGroup && Validator.isEmpty(data.courier)) {
        errors.courier = 'is required'
    }

    // payment
    data.payment = !isEmpty(data.payment) ? data.payment : 'cash'

    // discount
    data.discount = !isEmpty(data.discount) ? data.discount : 0

    // comment
    data.comment = !isEmpty(data.comment) ? data.comment : ''

    // notes
    data.notes = !isEmpty(data.notes) ? data.notes : []

    return {
        errors,
        isValid: isEmpty(errors),
    }
}
