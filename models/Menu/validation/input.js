const Validator = require('validator')
const isEmpty = require('../../../helpers/is-empty')

module.exports = function(data) {
    let errors = {}

    // num
    data.num = !isEmpty(data.num) ? data.num : 0

    // title
    data.title = !isEmpty(data.title) ? data.title : ''

    // isPeriodic
    data.isPeriodic = !isEmpty(data.isPeriodic) ? data.isPeriodic : false

    // date
    data.date = !isEmpty(data.date) ? data.date : ''
    if (!data.isPeriodic && Validator.isEmpty(data.date)) {
        errors.date = 'is required'
    }

    // dates
    data.dates = !isEmpty(data.dates) ? data.dates : []
    if (data.isPeriodic && data.date.length === 0) {
        errors.dates = 'is required'
    }

    // comment
    data.comment = !isEmpty(data.comment) ? data.comment : ''

    return {
        errors,
        isValid: isEmpty(errors),
    }
}
