const mongoose = require('mongoose')
const Schema = mongoose.Schema

const MenuSchema = new Schema({
    title: {
        type: String,
        default: '',
    },
    isPeriodic: {
        type: Boolean,
        default: false,
    },
    date: {
        type: Date,
        required: true,
    },
    dates: [],
    dishes: [
        // {
        //     dishId: { type: Schema.Types.ObjectId, ref: "dish" },
        //     num: {
        //         type: Number,
        //         require: true,
        //     },
        //     title: {
        //         type: String,
        //         default: "",
        //     },
        //     cardTitle: {
        //         type: String,
        //         required: true,
        //     },
        //     categoryId: {
        //         type: Schema.Types.ObjectId,
        //         ref: "category",
        //     },
        //     isComplex: {
        //         type: Boolean,
        //         default: false,
        //     },
        //     weight: {
        //         type: Number,
        //         require: true,
        //     },
        //     uomId: {
        //         type: Schema.Types.ObjectId,
        //         ref: "uom",
        //     },
        //     price: {
        //         type: Number,
        //         require: true,
        //     },
        // },
    ],
    comment: {
        type: String,
        default: '',
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
    created: {
        type: Date,
        default: new Date(),
    },
    updated: {
        type: Date,
        default: new Date(),
    },
})

module.exports = Menu = mongoose.model('menu', MenuSchema)
