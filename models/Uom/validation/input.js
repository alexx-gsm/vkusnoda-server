const Validator = require('validator')
const isEmpty = require('../../../helpers/is-empty')

module.exports = function(data) {
    let errors = {}

    // title
    data.title = !isEmpty(data.title) ? data.title : ''
    if (Validator.isEmpty(data.title)) {
        errors.title = 'title is required'
    }

    // abbr
    data.abbr = !isEmpty(data.abbr) ? data.abbr : ''
    if (Validator.isEmpty(data.abbr)) {
        errors.abbr = 'abbr is required'
    }

    // ref
    data.ref = !isEmpty(data.ref) ? data.ref : ''

    // amount
    data.amount = !isEmpty(data.amount) ? data.amount : 0

    return {
        errors,
        isValid: isEmpty(errors),
    }
}
