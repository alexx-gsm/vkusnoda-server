const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UomSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    abbr: {
        type: String,
        required: true,
    },
    ref: {
        type: Schema.Types.Mixed,
        ref: 'uom',
        default: '',
    },
    amount: {
        type: Number,
        default: 0,
    },

    isDeleted: {
        type: Boolean,
        default: false,
    },
    created: {
        type: Date,
        default: new Date(),
    },
    updated: {
        type: Date,
        default: new Date(),
    },
})

module.exports = Uom = mongoose.model('uom', UomSchema)
