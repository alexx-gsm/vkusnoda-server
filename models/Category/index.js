const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CategorySchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    parent: {
        type: String,
        default: '',
    },
    isGroup: {
        type: Boolean,
        default: false,
    },
    sorting: {
        type: Number,
        required: true,
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
    created: {
        type: Date,
        default: new Date(),
    },
    updated: {
        type: Date,
        default: new Date(),
    },
})

module.exports = Category = mongoose.model('category', CategorySchema)
