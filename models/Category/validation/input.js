const Validator = require('validator')
const isEmpty = require('../../../helpers/is-empty')

module.exports = function(data) {
    let errors = {}

    // title
    data.title = !isEmpty(data.title) ? data.title : ''
    if (!Validator.isLength(data.title, { min: 3, max: 50 })) {
        errors.title = 'title must have at least 3 chars'
    }
    if (Validator.isEmpty(data.title)) {
        errors.title = 'title is required'
    }

    // isGroup
    data.isGroup = !isEmpty(data.isGroup) ? data.isGroup : false

    // parent
    data.parent = !isEmpty(data.parent) ? data.parent : ''
    if (!data.isGroup && Validator.isEmpty(data.parent)) {
        errors.parent = 'parent is required'
    }

    // sorting
    data.sorting = !isEmpty(data.sorting) ? data.sorting : 100

    return {
        errors,
        isValid: isEmpty(errors),
    }
}
