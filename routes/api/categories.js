const express = require('express')
const passport = require('passport')

const router = express.Router()

// validations
const validateFormInput = require('../../models/Category/validation/input')

// model
const Category = require('../../models/Category')

/**
 * !    @route   POST api/categories
 * *    @desc    Save/Update category
 * ?    @access  Private
 */
router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { errors, isValid } = validateFormInput(req.body)
    if (!isValid) {
        return res.status(400).json(errors)
    }

    const { _id, ...categoryFields } = req.body

    try {
        if (_id) {
            const category = await Category.findOneAndUpdate(
                { _id },
                { $set: { ...categoryFields, updated: new Date() } },
                { new: true, upsert: true }
            )
            res.json({ category })
        } else {
            const newCategory = await new Category(categoryFields).save()
            res.json({ category: newCategory })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * !    @route   GET api/categories
 * *    @desc    Get all categories
 * ?    @access  Private
 */
router.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const categories = await Category.find({ isDeleted: false }).sort({ title: 1 })
        res.json({ categories })
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * ! @route   GET api/categories/:id
 * * @desc    Get category by id
 * ? @access  Private
 */
router.get('/:_id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { _id } = req.params
    try {
        if (_id) {
            const category = await Category.findById(_id)
            res.json({ category })
        } else {
            res.status(404).json({ error: 'not found' })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * ! @route   DELETE api/categories/:id
 * * @desc    Delete category (set "isDeleted" field to true)
 * ? @access  Private
 */
router.delete('/:_id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { _id } = req.params
    try {
        if (_id && req.user.role === 'admin') {
            await Category.findOneAndUpdate({ _id }, { $set: { isDeleted: true } })
            res.json({ category: 'is deleted' })
        } else {
            res.status(403).json({ error: 'Permission denied' })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

module.exports = router
