const express = require('express')
const passport = require('passport')

const router = express.Router()

// validations
const validateFormInput = require('../../models/Courier/validation/input')

// model
const Courier = require('../../models/Courier')

/**
 * !    @route   POST api/couriers
 * *    @desc    Save/Update courier
 * ?    @access  Private
 */
router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { errors, isValid } = validateFormInput(req.body)
    if (!isValid) {
        return res.status(400).json(errors)
    }

    const { _id, title } = req.body
    const courierFields = { title }

    try {
        if (_id) {
            const courier = await Courier.findOneAndUpdate(
                { _id },
                { $set: { ...courierFields, updated: new Date() } },
                { new: true, upsert: true }
            )
            res.json({ courier })
        } else {
            const newCourier = await new Courier(courierFields).save()
            res.json({ courier: newCourier })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * !    @route   GET api/couriers
 * *    @desc    Get all couriers
 * ?    @access  Private
 */
router.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const couriers = await Courier.find({ isDeleted: false }).sort({ title: 1 })
        res.json({ couriers })
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * ! @route   GET api/couriers/:id
 * * @desc    Get courier by id
 * ? @access  Private
 */
router.get('/:_id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { _id } = req.params
    try {
        if (_id) {
            const courier = await Courier.findById(_id)
            res.json({ courier })
        } else {
            res.status(404).json({ error: 'not found' })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * ! @route   DELETE api/couriers/:id
 * * @desc    Delete courier (set "isDeleted" field to true)
 * ? @access  Private
 */
router.delete('/:_id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { _id } = req.params
    try {
        if (_id && req.user.role === 'admin') {
            await Courier.findOneAndUpdate({ _id }, { $set: { isDeleted: true } })
            res.json({ courier: 'is deleted' })
        } else {
            res.status(403).json({ error: 'Permission denied' })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

module.exports = router
