const express = require('express')
const router = express.Router()
const gravatar = require('gravatar')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { secretOrKey } = require('../../config/keys')
const passport = require('passport')

// input Validations
const validateRegisterInput = require('../../models/User/validation/register')
const validateLoginInput = require('../../models/User/validation/login')
const validateUserInput = require('../../models/User/validation/user')

// User Model
const User = require('../../models/User')

/**
 * !    @route   POST api/users/register
 * *    @desc    Register user
 * ?    @access  Public
 */
router.post('/register', (req, res) => {
    const { errors, isValid } = validateRegisterInput(req.body)
    if (!isValid) {
        return res.status(400).json(errors)
    }

    const { email, password } = req.body

    User.findOne({ email }).then((user) => {
        if (user) {
            errors.email = 'Email already exists'

            return res.status(400).json(errors)
        } else {
            const avatar = gravatar.url(email, {
                s: '200', // Size
                r: 'pg', // Rating
                d: 'mm', // Default
            })

            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(password, salt, (err, hash) => {
                    if (err) throw err

                    const newUser = new User({
                        email,
                        hash,
                        role: 'admin',
                        avatar,
                    })

                    newUser
                        .save()
                        .then((user) => res.json(user))
                        .catch((err) => console.log(err))
                })
            })
        }
    })
})

/**
 * !    @route   GET api/users/login
 * *    @desc    Login User / Returning JWT Token
 * ?    @access  Public
 */
router.post('/login', (req, res) => {
    const { errors, isValid } = validateLoginInput(req.body)
    if (!isValid) {
        return res.status(400).json(errors)
    }

    const { email, password } = req.body

    // Find User by email
    User.findOne({ email }).then((user) => {
        if (!user) {
            errors.email = 'User not found'
            return res.status(404).json(errors)
        }

        bcrypt.compare(password, user.hash).then((isMatch) => {
            if (isMatch) {
                const payload = {
                    _id: user._id,
                    email: user.email,
                    avatar: user.avatar,
                    role: user.role,
                }

                jwt.sign(payload, secretOrKey, { expiresIn: 3600000 }, (err, token) => {
                    res.json({ token: 'Bearer ' + token })
                })
            } else {
                errors.password = 'Password incorrect'
                return res.status(400).json(errors)
            }
        })
    })
})

/**
 * !    @route   GET api/users/current
 * *    @desc    Return current user
 * ?    @access  Private
 */
router.get('/current', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const user = await User.findById(req.user._id)
        return res.json({ user })
    } catch (error) {
        return res.status(501).json({ errors: { server: 'Server error' } })
    }
})

/**
 *! @route   POST api/users
 ** @desc    Save/Update user (from admin)
 ** @access  Private
 */
router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { errors, isValid } = validateUserInput(req.body)
    // Check Validation
    if (!isValid) {
        return res.status(400).json(errors)
    }

    const { _id } = req.body
    const itemFields = {
        name: req.body.name,
        company: req.body.company,
        phone: req.body.phone,
        address: req.body.address,
        map: req.body.map,
        comment: req.body.comment,
        role: 'user',
    }

    try {
        if (_id && req.user.role === 'admin') {
            res.json(
                await User.findOneAndUpdate(
                    { _id },
                    { $set: itemFields },
                    { new: true, upsert: true }
                )
            )
        } else if (req.user.role === 'admin') {
            const newItem = new User(itemFields)
            res.json(await newItem.save())
        } else {
            res.status(403).json('ops')
        }
    } catch (error) {
        console.log('error', error)
        res.json(error)
    }
})

/**
 *! @route   POST api/users/all
 ** @desc    Get all users
 ** @access  Private
 */
router.post('/all', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        if (req.user.role === 'admin') {
            res.json(await User.find({ isDeleted: false }).sort({ name: 1 }))
        } else if (req.user.role === 'user') {
            res.json(await User.findOne({ id: req.user.id }))
        }
    } catch (error) {
        console.log('error', error)
        res.json(error)
    }
})

/**
 *! @route   POST api/customer/:id
 ** @desc    Get user by id
 ** @access  Private
 */
router.post('/:_id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { _id } = req.params
    try {
        if (_id && req.user.role === 'admin') {
            res.json(await User.findOne({ _id }))
        } else if (req.user.role === 'user') {
            res.json(await User.findOne({ id: req.user.id }))
        }
    } catch (error) {
        console.log('error', error)
        res.status(404).json(error)
    }
})

/**
 *! @route   DELETE api/customer/:id
 ** @desc    Delete user (mark user)
 ** @access  Private
 */
router.delete('/:_id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const { _id } = req.params
        if (_id && req.user.role === 'admin') {
            await User.findOneAndUpdate({ _id }, { $set: { isDeleted: true } })
            res.json('ok')
        } else {
            res.status(404).json('ops')
        }
    } catch (error) {
        console.log('error', error)
        res.status(404).json(error)
    }
})

module.exports = router
