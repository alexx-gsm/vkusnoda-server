const express = require('express')
const passport = require('passport')

const router = express.Router()

// validations
const validateFormInput = require('../../models/Uom/validation/input')

// model
const Uom = require('../../models/Uom')

/**
 * !    @route   POST api/uoms
 * *    @desc    Save/Update uom
 * ?    @access  Private
 */
router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { errors, isValid } = validateFormInput(req.body)
    if (!isValid) {
        return res.status(400).json(errors)
    }

    const { _id, ...uomFields } = req.body

    try {
        if (_id) {
            const uom = await Uom.findOneAndUpdate(
                { _id },
                { $set: { ...uomFields, updated: new Date() } },
                { new: true, upsert: true }
            )
            res.json({ uom })
        } else {
            const newUom = await new Uom(uomFields).save()
            res.json({ uom: newUom })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * !    @route   GET api/uoms
 * *    @desc    Get all uoms
 * ?    @access  Private
 */
router.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const uoms = await Uom.find({ isDeleted: false }).sort({ title: 1 })
        res.json({ uoms })
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * ! @route   GET api/uoms/:id
 * * @desc    Get uom by id
 * ? @access  Private
 */
router.get('/:_id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { _id } = req.params

    try {
        if (_id) {
            const uom = await Uom.findById(_id)
            res.json({ uom })
        } else {
            res.status(404).json({ error: 'not found' })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * ! @route   DELETE api/uoms/:id
 * * @desc    Delete uom (set "isDeleted" field to true)
 * ? @access  Private
 */
router.delete('/:_id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { _id } = req.params

    try {
        if (_id && req.user.role === 'admin') {
            await Uom.findOneAndUpdate({ _id }, { $set: { isDeleted: true } })
            res.json({ uom: 'is deleted' })
        } else {
            res.status(403).json({ error: 'Permission denied' })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

module.exports = router
