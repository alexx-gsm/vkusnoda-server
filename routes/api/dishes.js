const express = require('express')
const passport = require('passport')

const router = express.Router()

// validations
const validateFormInput = require('../../models/Dish/validation/input')

// model
const Dish = require('../../models/Dish')

/**
 * !    @route   POST api/dishes
 * *    @desc    Save/Update dish
 * ?    @access  Private
 */
router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { errors, isValid } = validateFormInput(req.body)
    if (!isValid) {
        return res.status(400).json(errors)
    }

    const { _id, ...dishFields } = req.body

    try {
        if (_id) {
            const dish = await Dish.findOneAndUpdate(
                { _id },
                { $set: { ...dishFields, updated: new Date() } },
                { new: true, upsert: true }
            )
            res.json({ dish })
        } else {
            const newDish = await new Dish(dishFields).save()
            res.json({ dish: newDish })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * !    @route   GET api/dishes
 * *    @desc    Get all dishes
 * ?    @access  Private
 */
router.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const dishes = await Dish.find({ isDeleted: false }).sort({ title: 1 })
        res.json({ dishes })
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * ! @route   GET api/dishes/:id
 * * @desc    Get dish by id
 * ? @access  Private
 */
router.get('/:_id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { _id } = req.params
    try {
        if (_id) {
            const dish = await Dish.findById(_id)
            res.json({ dish })
        } else {
            res.status(404).json({ error: 'not found' })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * ! @route   DELETE api/dishes/:id
 * * @desc    Delete dish (set "isDeleted" field to true)
 * ? @access  Private
 */
router.delete('/:_id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { _id } = req.params
    try {
        if (_id && req.user.role === 'admin') {
            await Dish.findOneAndUpdate({ _id }, { $set: { isDeleted: true } })
            res.json({ dish: 'is deleted' })
        } else {
            res.status(403).json({ error: 'Permission denied' })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

module.exports = router
