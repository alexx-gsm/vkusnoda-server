const express = require('express')
const passport = require('passport')

const router = express.Router()

// validations
const validateFormInput = require('../../models/Client/validation/input')

// model
const Client = require('../../models/Client')

/**
 * !    @route   POST api/clients
 * *    @desc    Save/Update client
 * ?    @access  Private
 */
router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { errors, isValid } = validateFormInput(req.body)
    console.log('===')

    if (!isValid) {
        return res.status(400).json(errors)
    }

    const { _id, ...clientFields } = req.body

    try {
        if (_id) {
            const client = await Client.findOneAndUpdate(
                { _id },
                { $set: { ...clientFields, updated: new Date() } },
                { new: true, upsert: true }
            )
            res.json({ client })
        } else {
            console.log('-->')
            const number = await Client.count()
            console.log('<--')
            const newClient = await new Client({ ...clientFields, number }).save()
            res.json({ client: newClient })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * !    @route   GET api/clients
 * *    @desc    Get all clients
 * ?    @access  Private
 */
router.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const clients = await Client.find({ isDeleted: false }).sort({ name: 1 })
        res.json({ clients })
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * !    @route   GET api/clients/groups
 * *    @desc    Get all client-groups
 * ?    @access  Private
 */
router.get('/groups', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const groups = await Client.find({ isGroup: true, isDeleted: false }).sort({ name: 1 })
        res.json({ groups })
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * ! @route   GET api/clients/:id
 * * @desc    Get client by id
 * ? @access  Private
 */
router.get('/:_id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { _id } = req.params
    try {
        if (_id) {
            const client = await Client.findById(_id)
            res.json({ client })
        } else {
            res.status(404).json({ error: 'not found' })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

/**
 * ! @route   DELETE api/clients/:id
 * * @desc    Delete client (set "isDeleted" field to true)
 * ? @access  Private
 */
router.delete('/:_id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    const { _id } = req.params
    try {
        if (_id && req.user.role === 'admin') {
            await Client.findOneAndUpdate({ _id }, { $set: { isDeleted: true } })
            res.json({ client: 'is deleted' })
        } else {
            res.status(403).json({ error: 'Permission denied' })
        }
    } catch (error) {
        console.log('error', error)
        res.status(500).json(error)
    }
})

module.exports = router
