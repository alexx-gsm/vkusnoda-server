module.exports = (app) => {
    app.use('/api/users', require('./api/users'))
    app.use('/api/couriers', require('./api/couriers'))
    app.use('/api/clients', require('./api/clients'))
    app.use('/api/categories', require('./api/categories'))
    app.use('/api/uoms', require('./api/uoms'))
    app.use('/api/dishes', require('./api/dishes'))
}
